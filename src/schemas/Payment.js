const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const paymentsSchema = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        ref: 'Payment'
    },
    date: {
        type: Date,
    },
    time: {
        type: String,
    },
    userID: {
        type: String,
    },
    recordID: {
        type: String,
    },
    amount: {
        type: String,
    },
});

module.exports = mongoose.model('Payment', paymentsSchema);