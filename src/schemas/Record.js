const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const generateCode = () => Math.random().toString(36).substring(6).toUpperCase();

const file = new Schema({
    id: String,
    fileName: String,
    path: String,
    name: String,
    size: String,
});

const payment = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        ref: 'Payment'
    },
    date: {
        type: Date,
    },
    userID: {
        type: String,
    },
    placeOfWork: {
        type: String,
    },
    fullName: {
        type: String,
    },
    amount: {
        type: Number,
    },
});

const comment = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    },
    userID: {
        type: String,
    },
    date: {
        type: String,
    },
    message: {
        type: String,
    },
});

const RecordsSchema = new Schema({
    ID: {
        type: String,
        default: generateCode,
    },
    product: {
        type: String,
        required: true,
    },
    firstName: {
        type: String,
        minlength: 1,
        maxlength: 30,
        required: true,
    },
    lastName: {
        type: String,
        minlength: 1,
        maxlength: 30,
        required: true,
    },
    fullName: {
        type: String,
    },
    departureDate: {
        type: Date,
        required: true,
    },
    returnDate: {
        type: Date,
    },
    departureCity: {
        type: String,
        required: true,
    },
    destinationCity: {
        type: String,
        required: true,
    },
    airline: {
        type: String,
        required: true,
    },
    remarkPublic: {
        type: String,
    },
    remarkRC: {
        type: String,
    },
    creationDate: {
        type: Date,
    },
    dateValidity: {
        type: String,
    },
    amountTotal: {
        type: Number,
        required: true,
    },
    amountToPay: {
        type: Number,
        required: true,
    },
    amountAlreadyPaid: {
        type: Number,
        required: true,
    },
    partialPayment: {
        type: Boolean,
    },
    files: {
        type: [file],
    },
    paymentObject: {
        type: [payment],
    },
    commentsObject: {
        type: [comment],
    },
    status: {
        default: 'valid',
        type: String,
    },
});

// RecordsSchema.virtual('fullName')
//     .get(function () {
//         return this.firstName + ' ' + this.lastName;
//     });


module.exports = mongoose.model('Record', RecordsSchema);
