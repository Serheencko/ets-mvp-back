const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const commentsSchema = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    },
    userID: {
        type: String,
    },
    recordID: {
        type: String,
    },
    date: {
        type: String,
    },
    time: {
        type: String,
    },
    message: {
        type: String,
    },
});

module.exports = mongoose.model('Comment', commentsSchema);