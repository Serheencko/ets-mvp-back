const authenticate = require('../../middleware/authenticate');
const errors = require('../../errors');
const router = require('express').Router();
const Record = require('../../schemas/Record');
const roles = require('../../../enum/roles');

/**
 *  @swagger
 *  /record:
 *    get:
 *      tags:
 *        - record
 *      description: get record record
 *      parameters:
 *        - name: id
 *          description: record primary key
 *          in: path
 *          type: string
 *          default: test
 *          required: true
 *      responses:
 *        200:
 *          description: record received
 */

router.get('/records',
    authenticate([roles.EASE]),
    errors.wrap(async (req, res) => {
        const {query} = req;
        const filter = {};
        
        if ('ID' in query) {
            filter.ID = new RegExp(`${query.ID}`, 'i');
        }
        
        if ('name' in query) {
            filter.fullName = new RegExp(`${query.name}`, 'i');
        }
        
        if ('destination' in query) {
            filter.destinationCity = new RegExp(`${query.destination}`, 'i');
        }
        
        const record = await Record.find(filter);
        
        if (!record) throw errors.NotFoundError('Record not found');
        
        res.status(200).send(record);
    })
);

module.exports = router;
