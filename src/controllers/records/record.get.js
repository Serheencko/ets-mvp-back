const authenticate = require('../../middleware/authenticate');
const errors = require('../../errors');
const router = require('express').Router();
const Record = require('../../schemas/Record');
const roles = require('../../../enum/roles');
/**
 *  @swagger
 *  /record/{id}:
 *    get:
 *      tags:
 *        - record
 *      description: get record record
 *      parameters:
 *        - name: id
 *          description: record primary key
 *          in: path
 *          type: string
 *          default: test
 *          required: true
 *      responses:
 *        200:
 *          description: record received
 */

router.get('/record/:id',
    authenticate(),
    errors.wrap(async (req, res) => {
        const record = await Record.find({ID: req.params.id});
        if (!record) throw errors.NotFoundError('Record not found');
        if (res.locals.user.role === roles.PARTNER) record[0].remarkRC = undefined;
        res.status(200).send(record);
    })
);

module.exports = router;
