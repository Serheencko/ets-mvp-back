const router = require('express').Router();
const multer = require('multer');
const upload = multer({dest: './public/files/'});

const errors = require('../../errors');
const authenticate = require('../../middleware/authenticate');
const Record = require('../../schemas/Record');
const roles = require('../../../enum/roles');

const emailHelper = require('../../helpers/email-helper');
const arrangeInput = require('../../middleware/arrange-inputs');


/**
 *  @swagger
 *  /record:
 *    post:
 *      tags:
 *        - record
 *      description: save report
 *      parameters:
 *        - name: value
 *          default: value
 *          required: true
 *          in: formData
 *          type: string
 *      responses:
 *        200:
 *          description: return saved report object
 */

router.post('/record',
    authenticate([roles.EASE]),
    upload.array('recordFile'),
    arrangeInput('body', {
        product: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-]{2,30}/,
        },
        firstName: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-]{1,30}/,
        },
        lastName: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-]{2,30}/,
        },
        departureCity: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-]{2,30}/,
        },
        destinationCity: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-]{2,30}/,
        },
        departureDate: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-]{2,20}/,
        },
        returnDate: {
            type: 'STRING',
            pattern: /[a-zA-Z0-9._%+-]{0,20}/,
        },
        airline: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-]{2,40}/,
        },
        amountTotal: {
            type: 'NUMBER',
            required: true,
            pattern: /[0-9.]{1,12}/,
        },
        amountAlreadyPaid: {
            type: 'NUMBER',
            required: true,
            pattern: /[0-9.]{1,12}/,
        },
        amountToPay: {
            type: 'NUMBER',
            required: true,
            pattern: /[0-9.]{1,12}/,
        },
        partialPayment: {
            required: true,
        },
    }),
    
    errors.wrap(async (req, res) => {
        const recordFiles = req.files.map(file => file = {
            path: `/files/${file.filename}`,
            fileName: file.filename, name: file.originalname, size: file.size
        });
        const record = req.body;
        record.fullName = `${req.body.firstName} ${req.body.lastName}`;
        record.files = recordFiles;
        
        
        if (record.amountTotal <= 0) throw errors.NotFoundError('Total amount should be greater than 0');
        if (record.amountAlreadyPaid < 0) throw errors.NotFoundError('Amount paid should be equal or greater than 0');
        if (record.amountToPay === '0') record.status = 'paid';

        const createdRecord = await Record.create(record);
        if (!createdRecord) throw errors.NotFoundError('Creation error');

        await emailHelper.sendEmail(process.env.RECIEVER_EMAIL, {
            subject: `NEW RECORD - ${createdRecord.product} - ${createdRecord.ID} - ${createdRecord.amountTotal} XAF`,
            type: 'record',
            title: 'New record was created',
            header: `NEW RECORD - ${createdRecord.product} - ${createdRecord.ID} - ${createdRecord.amountTotal} XAF`,
            product: `${createdRecord.product}`,
            recordID: `${createdRecord.ID}`,
            name: `${createdRecord.fullName}`,
            airline: `${createdRecord.airline}`,
            departure: `${createdRecord.departureDate} ,  ${createdRecord.departureCity}`,
            destination: `${createdRecord.destinationCity}`,
            phoneNumber: `${res.locals.user.phoneNumber}`,
            totalAmount: `${createdRecord.amountTotal}`,
        });

        res.json(createdRecord);
    })
);

module.exports = router;
