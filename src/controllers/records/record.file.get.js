const authenticate = require('../../middleware/authenticate');
const errors = require('../../errors');
const router = require('express').Router();
/**
 *  @swagger
 *  /record/download:
 *    get:
 *      tags:
 *        - file
 *      description: get record file
 *      parameters:
 *        - name: id
 *          description: record primary key
 *          in: path
 *          type: string
 *          default: test
 *          required: true
 *      responses:
 *        200:
 *          description: file received
 */

router.get('/record/download/:id',
    authenticate(),
    errors.wrap(async (req, res) => {
        const file = `./public/files/${req.params.id}`;
        res.status(200).download(file); 
        
        if (!file) throw errors.NotFoundError('File not found');
    
        
    })
);

module.exports = router;