const router = require('express').Router();
const multer = require('multer');
const upload = multer({dest: './public/files/'});

const errors = require('../../errors');
const authenticate = require('../../middleware/authenticate');
const Record = require('../../schemas/Record');
const roles = require('../../../enum/roles');

/**
 *  @swagger
 *  /record:
 *    patch:
 *      tags:
 *        - record
 *      description: save report
 *      parameters:
 *        - name: name
 *          default: ReportName
 *          required: true
 *          in: formData
 *          type: string
 *      responses:
 *        200:
 *          description: return saved report object
 */

router.put('/record/:id',
    authenticate([roles.EASE]),
    upload.single('recordFile'),
    errors.wrap(async (req, res) => {
        const {params: {id}} = req;
        const newFile = {path: `/files/${req.file.filename}`, 
            fileName: req.file.filename, name: req.file.originalname, size: req.file.size};
        const record = await Record.findOne({ID: id});
        record.files.push(newFile);
        const recordPut = await Record.findByIdAndUpdate({_id: record._id}, {files: record.files});
        
        if (!recordPut) throw errors.NotFoundError('Put error');
        
        res.json(record);
    })
);

module.exports = router;
