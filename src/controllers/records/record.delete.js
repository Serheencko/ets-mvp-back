const authenticate = require('../../middleware/authenticate');
const errors = require('../../errors');
const router = require('express').Router();
const Record = require('../../schemas/Record');

/**
 *  @swagger
 *  /record/{id}:
 *    delete:
 *      tags:
 *        - record
 *      description: delete record
 *      parameters:
 *        - name: id
 *          description: record primary key
 *          in: path
 *          type: string
 *          default: test
 *          required: true
 *      responses:
 *        204:
 *          description: record was deleted
 */

router.delete('/record/:id',
    authenticate(),
    errors.wrap(async (req, res) => {
        const {params: {id}} = req;
        const recordDelete = await Record.deleteOne({_id: id});
        
        if (recordDelete.n === 0) throw errors.NotFoundError('Record does not exist')
        
        res.json(recordDelete);
    })
);

module.exports = router;