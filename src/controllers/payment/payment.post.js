const router = require('express').Router();
const errors = require('../../errors');

const authenticate = require('../../middleware/authenticate');
const arrangeInput = require('../../middleware/arrange-inputs');

const Record = require('../../schemas/Record');
const roles = require('../../../enum/roles');
const emailHelper = require('../../helpers/email-helper');
/**
 *  @swagger
 *  /payment/{id}:
 *    post:
 *      tags:
 *        - payment
 *      description: create payment
 *      parameters:
 *        - name: name
 *          default: ReportName
 *          required: true
 *          in: formData
 *          type: string
 *      responses:
 *        200:
 *          description: return saved report object
 */

router.post('/payment/:id',
    authenticate([roles.PARTNER]),
    arrangeInput('body', {
        paymentAmount: {
            type: 'NUMBER',
            required: true,
            pattern: /[0-9.]{1,12}/,
        },
    }),
    errors.wrap(async (req, res) => {
        const user = res.locals.user;
        const paymentAmount = Number(req.body.paymentAmount);
        const record = await Record.findOne({ID: req.params.id});

        if (!record) throw errors.NotFoundError('Record not found!');
        if (paymentAmount <= 0) throw errors.NotFoundError('Amount should be greater than 0!');
        if (record.partialPayment === false && paymentAmount < record.amountToPay) {
            throw errors.NotFoundError('insuficcient payment!');
        };
        if (paymentAmount > record.amountToPay) throw errors.NotFoundError('Amount can not be greater than amount to pay!');

        const paymentObject = {
            userID: user._id,
            amount: paymentAmount,
            placeOfWork: user.placeOfWork,
            fullName: user.fullName,
            date: new Date(),
        };

        const amountAlreadyPaid = record.amountAlreadyPaid + paymentAmount;
        const amountToPay = record.amountTotal - amountAlreadyPaid;

        if (amountToPay === 0) record.status = 'paid';

        record.paymentObject.push(paymentObject);
        record.amountAlreadyPaid = amountAlreadyPaid;
        record.amountToPay = amountToPay;

        await record.save();
        if (amountToPay === 0) {
            await emailHelper.sendEmail(process.env.RECIEVER_EMAIL, {
                subject: `Payment was successfully completed, amount ${amountAlreadyPaid} XAF`,
                type: 'payment complete',
                title: 'Payment completed',
                header: `Payment was successfully completed, amount ${amountAlreadyPaid} XAF`,
                recordID: `${record.ID}`,
                fullName: `${record.fullName}`,
                paymentComplete: `${amountAlreadyPaid}`,
                dateAndTimeOfLastPayment: `${paymentObject.date}`,
            });
        } else {
            await emailHelper.sendEmail(process.env.RECIEVER_EMAIL, {
                subject: `Payment successfully created, amount ${paymentAmount} XAF`,
                type: 'any payment',
                title: 'Payment was done',
                header: `Payment was successfully created, amount ${paymentAmount} XAF`,
                product: `${record.product}`,
                recordID: `${record.ID}`,
                fullName: `${record.fullName}`,
                phoneNumber: `${res.locals.user.phoneNumber}`,
                totalAmount: `${record.amountTotal}`,
                newPayment: `${paymentAmount}`,
                totalPaid: `${amountAlreadyPaid}`,
                amountToPay: `${amountToPay}`,
            });
        }

        res.json(record);
    })
);

module.exports = router;
