const authenticate = require('../../middleware/authenticate');
const errors = require('../../errors');
const router = require('express').Router();
const User = require('../../schemas/User');
/**
 *  @swagger
 *  /user/{id}:
 *    get:
 *      tags:
 *        - user
 *      description: get users record
 *      parameters:
 *        - name: id
 *          description: users primary key
 *          in: path
 *          type: string
 *          default: test
 *          required: true
 *      responses:
 *        200:
 *          description: users received
 */

router.get('/user/:id',
    authenticate(),
    errors.wrap(async (req, res) => {
        const id = req.params.id;
        const user = await User.find({ _id: id  });
        
        if (!user) throw errors.NotFoundError('User not found');
    
        res.status(200).send(user);
    })
);

module.exports = router;
