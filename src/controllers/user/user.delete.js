const authenticate = require('../../middleware/authenticate');
const errors = require('../../errors');
const router = require('express').Router();
const User = require('../../schemas/User');
const roles = require('../../../enum/roles');
/**
 *  @swagger
 *  /user/{id}:
 *    delete:
 *      tags:
 *        - user
 *      description: delete user
 *      parameters:
 *        - name: id
 *          description: user primary key
 *          in: path
 *          type: string
 *          default: test
 *          required: true
 *      responses:
 *        204:
 *          description: user was deleted
 */

router.delete('/user/:id',
    authenticate([roles.ADMIN]),
    errors.wrap(async (req, res) => {
        const { params: { id } } = req;
        const userDelete = await User.deleteOne({ _id: id });
        
        if (userDelete.n === 0) throw errors.NotFoundError('User does not exist')
    
        res.json(userDelete);
    })
);

module.exports = router;
