const router = require('express').Router();

const errors = require('../../errors');
const authenticate = require('../../middleware/authenticate');
const arrangeInput = require('../../middleware/arrange-inputs');
const emailHelper = require('../../helpers/email-helper');

const User = require('../../schemas/User');
const roles = require('../../../enum/roles');
/**
 *  @swagger
 *  /user:
 *    post:
 *      tags:
 *        - user
 *      description: save report
 *      parameters:
 *        - name: value
 *          default: value
 *          required: true
 *          in: formData
 *          type: string
 *      responses:
 *        200:
 *          description: return saved report object
 */

router.post('/user',
    authenticate([roles.ADMIN]),
    arrangeInput('body', {
        firstName: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-]{1,30}/,
        },
        lastName: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-]{2,30}/,
        },
        password: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-;&!@?''""<>*$#]{6,30}/,
        },
        passwordCheck: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-;&!@?''""<>*$#]{6,30}/,
        },
        placeOfWork: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-;&!@?''""<>*$#]{2,50}/,
        },
        workPlaceID: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-;&!@?''""<>*$#]{2,20}/,
        },
        phoneNumber: {
            type: 'STRING',
            required: true,
            pattern: /[0-9._%+-]{2,15}/,
        },
        city: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-]{2,50}/,
        },
        email: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}/,
        },
        role: {
            type: 'STRING',
            required: true,
        },
    }),
    errors.wrap(async (req, res) => {
        const body = req.body;
        
        const existingUser = await User.findOne({$or: [
            {email: body.email},
            {phoneNumber: body.phoneNumber}
        ]});
        
        
        
        if (!(body.password === body.passwordCheck)) throw errors.InvalidInputError(`Passwords doesn't match`);
        
        if (existingUser) throw errors.InvalidInputError('User with same Email or Phone already exist!');
        const user = await User.create({...body, fullName: `${body.firstName} ${body.lastName}`});
        
        if (!user) throw errors.NotFoundError('User creation error');

        await emailHelper.sendEmail(user.email, {
            subject: `NEW ACCOUNT WAS CREATED FOR YOU`,
            type: 'user',
            title: 'NEW ACCOUNT WAS CREATED FOR YOU',
            header: `${user.fullName}, Your new account was created in ${new Date} via ETS`,
            name: `${user.fullName}`,
            userID: `${user.ID}`,
            phoneNumber: `${user.phoneNumber}`,
            password: `${body.password}`,
            role: `${user.role}`,
            placeOfWork: `${user.placeOfWork}`,
        });

        res.json(user);
    })
);

module.exports = router;
