const authenticate = require('../../middleware/authenticate');
const errors = require('../../errors');
const router = require('express').Router();
const User = require('../../schemas/User');
/**
 *  @swagger
 *  /users:
 *    get:
 *      tags:
 *        - users
 *      description: get users record
 *      parameters:
 *        - name: id
 *          description: users primary key
 *          in: path
 *          type: string
 *          default: test
 *          required: true
 *      responses:
 *        200:
 *          description: users received
 */

router.get('/users',
    authenticate(),
    errors.wrap(async (req, res) => {
        const {query} = req;
        const filter = {};
        
        if ('name' in query) {
            filter.fullName = new RegExp(`${query.name}`, 'i')
        }
        if ('role' in query) {
            filter.role = new RegExp(`${query.role}`, 'i')
        }
        if ('city' in query) {
            filter.city = new RegExp(`${query.city}`, 'i')
        }
        if ('place_of_work' in query) {
            filter.placeOfWork = new RegExp(`${query.place_of_work}`, 'i')
        }
        
        const user = await User.find(filter);
        
        if (!user) throw errors.NotFoundError('User not found');
        
        res.status(200).send(user);
    })
);

module.exports = router;
