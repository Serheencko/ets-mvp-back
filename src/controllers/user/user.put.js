const router = require('express').Router();
const errors = require('../../errors');
const authenticate = require('../../middleware/authenticate');
const User = require('../../schemas/User');
const roles = require('../../../enum/roles');
/**
 *  @swagger
 *  /user:
 *    patch:
 *      tags:
 *        - user
 *      description: save report
 *      parameters:
 *        - name: name
 *          default: ReportName
 *          required: true
 *          in: formData
 *          type: string
 *      responses:
 *        200:
 *          description: return saved report object
 */

router.put('/user/:id',
    authenticate([roles.ADMIN]),
    errors.wrap(async (req, res) => {
        const {params: {id}} = req;
        
        const userPut = await User.findByIdAndUpdate({_id: id}, req.body);
        
        if (!userPut) throw errors.NotFoundError('Put error')
        
        res.json(userPut);
    })
);

module.exports = router;
