const router = require('express').Router();

const errors = require('../../errors');
const User = require('../../schemas/User');
const arrangeInput = require('../../middleware/arrange-inputs');
/**
 *  @swagger
 *  /login:
 *    post:
 *      tags:
 *        - auth
 *      description: login user in system
 *      parameters:
 *        - name: email
 *          default: email@example.com
 *          required: true
 *          in: formData
 *          type: string
 *        - name: password
 *          default: password
 *          required: true
 *          in: formData
 *          type: string
 *      responses:
 *        200:
 *          description: return saved report object
 */

router.post('/login',
    arrangeInput('body', {
        login: {
            type: 'STRING',
            required: true,
            pattern: /[a-zA-Z0-9._%+-]{2,20}/,
        },
        password: {
            type: 'STRING',
            required: true,
        },
    }),
    errors.wrap(async (req, res) => {
        let user = await User.authenticate(req.body.login, req.body.password);
        if (!user) throw errors.NotFoundError('User not found or invalid credentials.');
        const token = await user.generateToken();
        user = user.toObject();
        delete user.password;
        res.json({user, token});
    })
);

module.exports = router;
