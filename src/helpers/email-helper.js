const nodemailer = require('nodemailer');
const pug = require('pug');

module.exports = {
    
    async sendEmail(recepient, data) {
        if (!(process.env.SEND_EMAILS === 'TRUE')) return;
        console.log('===================');
        let templateFile = '';
        switch (data.type) {
        case 'record':
            templateFile = 'create-record.pug';
            break;
        case 'user':
            templateFile = 'create-user.pug';
            break;
        case 'any payment':
            templateFile = 'create-any-payment.pug';
            break;
        case 'payment complete':
            templateFile = 'complete-payment.pug';
            break;
        }
        const emailTemplate = pug.compileFile(`${process.cwd()}/src/email-templates/${templateFile}`);
        const emailBody = emailTemplate({data});
        
        
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.NOTOFICATION_ACCOUNT_EMAIL,
                pass: process.env.NOIIFICATION_ACCOUNT_PASSWORD,
            }
        });
        
        const mailOptions = {
            // sender address
            from: 'ETC MVP',
            // list of receivers
            to: [recepient],
            // Subject line
            subject: data.subject,
            // plain text body
            html: emailBody,
        };
        
        try {
            await transporter.sendMail(mailOptions);
            console.log('>>> Message successfully sent "sent"');
        } catch (e) {
            console.log('>>> Message failed to send');
            console.log(e);
        }
    }
    
};
