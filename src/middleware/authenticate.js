'use strict';
const jwt = require('jsonwebtoken');
const errors = require('../errors');
const User = require('../schemas/User');
const userRoles = require('../../enum/roles')
module.exports = function authenticate(roles) {
    return errors.wrap(async function (req, res, next) {
        if (!('authorization' in req.headers)) return res.status(403).send('Missing authorization header');
        const token = req.headers['authorization'].split(' ')[1];
        let payload;
        
        try {
            payload = jwt.verify(token, process.env.SALT || 'salt');
        } catch (err) {
            throw errors.UnauthorizedError(err.name);
        }
        
        const user = await User.findById(payload.userId);
        if (!user) throw errors.UnauthorizedError('User not found or invalid credentials');
        
        /** check available user roles */
        if (roles && user.role !== userRoles.ADMIN && !roles.includes(user.role)) throw errors.UnauthorizedError(`Access denied for user with role "${user.role}"`);
        
        res.locals.user = user;
        next();
    });
};
