## Server

- [api-docs](https://exapmle.app/api-docs/#/)

### Installation

Server:
```
npm install
npm run serve
```
### Environment

#### App setup


#### Database credentials
DB_HOST=[db_host]
DB_USER=[db_user]
DB_PASSWORD=[dp_pass]
DB_NAME=[db_name]

### Development

#### Naming Conventions

##### Use lowerCamelCase for variables, properties and function names

Variables, properties and function names should use `lowerCamelCase`.  They
should also be descriptive. Single character variables and uncommon
abbreviations should generally be avoided.

##### Use UpperCamelCase for class names

Class names should be capitalized using `UpperCamelCase`.


##### Use UPPERCASE for Constants

Constants should be declared as regular variables or static class properties,
using all uppercase letters.

##### Use kebab-case for file names.

There is no official rule that you have to follow while naming your js file,
but the practice of using a hyphenated name like "some-name.js" is the most widely followed naming convention.

##### Mailings credentials.

There are variables in .env  file. 

RECIEVER_EMAIL is for email account that recieve emails. 

NOTOFICATION_ACCOUNT_EMAIL  this is email from alert account which will send alerts

NOIIFICATION_ACCOUNT_PASSWORD  there should be password for alert account

SEND_EMAILS = TRUE   that variable is for email helper, for turning on mailing