module.exports = {
    db: {
        URI: process.env.DB_URI,
    },
    app: {
        port: process.env.PORT || 3000,
    },
};
